package HttpResponse;

import com.jayway.restassured.response.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.jayway.restassured.RestAssured.get;

public class TestHttpResponse {
    private Response response;
    private Document responseBody;
    private Element findableElement;

    @BeforeTest
    public void setup() {
        response = get("https://habr.com/post/121234/");
        responseBody = Jsoup.parse(response.getBody().asString());
    }

    @Test
    public void testResponseStatusCode() {
        Assert.assertEquals(response.getStatusCode(), 200);
        Reporter.log("Response Status code was: " + response.getStatusCode() + " and expected result was: " + 200) ;
    }

    @Test
    public void testIfElementExists() {
        Assert.assertNotNull(responseBody.getElementById("post_121234"));
    }

    @Test
    public void testContentType() {

        Assert.assertEquals(response.getContentType(),"text/html; charset=UTF-8", "Such element with such id doesn't exist");
        Reporter.log("Content Type is: " +  response.getContentType());
    }

}
