package org.snickone77.calculator;

import org.testng.annotations.DataProvider;

public class StaticProvider {
    @DataProvider
    public Object[][] getData() {

        return new Object[][]{{10, 5}, {6, 0}};
    }
}
