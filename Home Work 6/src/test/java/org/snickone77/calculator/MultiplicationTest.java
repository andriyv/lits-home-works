package org.snickone77.calculator;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class MultiplicationTest {
    @Test(dataProvider = "getData", dataProviderClass = StaticProvider.class)
    public void testMultiplicationDataProvider(int a, int b)  {
        Assert.assertEquals(Calculator.multiply(a, b), a * b);
    }

    @Parameters({"param1","param2"})
    @Test
    public void testMultiplicationParams(int a, int b){
        Assert.assertEquals(Calculator.multiply(a,b), a*b);
    }

    @Test
    public void testMultiplication(){
        Assert.assertEquals(Calculator.multiply(5,7), 35);
    }

    @Test
    public void MultiplicationSoftAssert(){
        SoftAssert softAssertion= new SoftAssert();
        System.out.println("softAssert Method Was Started");
        softAssertion.assertEquals(Calculator.multiply(5,3),16);
        softAssertion.assertEquals(Calculator.multiply(5,3),15);
        System.out.println("softAssert Method Was Executed");
    }

}
