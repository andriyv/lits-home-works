package org.snickone77.calculator;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class DivisionTest {
    @Test(dataProvider = "getData", dataProviderClass = StaticProvider.class)
    public void testDivisionDataProvider(int a, int b)  {
        try {
            Assert.assertEquals(Calculator.divide(a, b), a/b);
        } catch (ArithmeticException ex){
            System.out.println("You have tried to divide by zero");
        }

    }

    @Parameters({"param1","param2"})
    @Test
    public void testDivisionParams(int a, int b){
        try {
            Assert.assertEquals(Calculator.divide(a,b), a/b);
        } catch (ArithmeticException ex){
            System.out.println("You have tried to divide by zero");
        }

    }

    @Test
    public void testDivision(){
        Assert.assertEquals(Calculator.divide(63,7), 9);
    }

    @Test
    public void DivisionSoftAssert(){
        SoftAssert softAssertion= new SoftAssert();
        System.out.println("softAssert Method Was Started");
        softAssertion.assertEquals(Calculator.divide(5,3),3);
        softAssertion.assertEquals(Calculator.divide(5,3),1);
        System.out.println("softAssert Method Was Executed");
    }

}
