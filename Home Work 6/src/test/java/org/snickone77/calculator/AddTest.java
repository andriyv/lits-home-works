package org.snickone77.calculator;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AddTest {

    @Test(dataProvider = "getData", dataProviderClass = StaticProvider.class)
    public void testAddDataProvider(int a, int b)  {
        Assert.assertEquals(Calculator.add(a, b), a + b);
    }

    @Parameters({"param1","param2"})
    @Test
    public void testAddParams(int a, int b){
        Assert.assertEquals(Calculator.add(a,b), a + b);
    }

    @Test
    public void testAdd(){
        Assert.assertEquals(Calculator.add(5,7), 12);
    }

    @Test
    public void AddSoftAssert(){
        SoftAssert softAssertion= new SoftAssert();
        System.out.println("softAssert Method Was Started");
        softAssertion.assertEquals(Calculator.add(5,3),15);
        System.out.println("softAssert Method Was Executed");
    }



}
