package org.snickone77.calculator;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SubtractionTest {
    @Test(dataProvider = "getData", dataProviderClass = StaticProvider.class)
    public void testSubtratcionDataProvider(int a, int b)  {
        Assert.assertEquals(Calculator.subtract(a, b), a - b);
    }

    @Parameters({"param1","param2"})
    @Test
    public void testSubtractionParams(int a, int b){
        Assert.assertEquals(Calculator.subtract(a,b), a - b);
    }

    @Test
    public void testSubtraction(){
        Assert.assertEquals(Calculator.subtract(5,7), 5 - 7);
    }

    @Test
    public void SubtractionSoftAssert(){
        SoftAssert softAssertion= new SoftAssert();
        System.out.println("softAssert Method Was Started");
        softAssertion.assertEquals(Calculator.subtract(5,3),15);
        System.out.println("softAssert Method Was Executed");
    }

}
