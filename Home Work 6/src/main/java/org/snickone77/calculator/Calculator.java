package org.snickone77.calculator;

import jdk.nashorn.internal.runtime.Undefined;

public class Calculator {

    public static int add(int a, int b){
        return a+b;
    }
    public static int subtract(int a, int b){
        return a - b;
    }
    public static int multiply(int a, int b){
        return a * b;
    }
    public static int divide(int a, int b){
        Integer result;
        try{
            result = a/b;
        }
        catch (ArithmeticException ex){
            result = 0;
        }
        return result;
    }

}
