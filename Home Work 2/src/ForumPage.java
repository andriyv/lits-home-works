public class ForumPage extends Page implements IChrome {
    public ForumPage(String url, String htmlContent) {
        super("Forum Page", url, htmlContent);
    }

    @Override
    public String getHtmlChrome() {
        return "Chrome";
    }

    @Override
    public String htmlBuilder() {

        return getHtmlChrome() + "\n" +
                "mysite.com" +  getUrl() +"\n"  +
                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title> " + getTitle() + "</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p> " + getHtmlContent() + " </p>\n" +
                "</body>\n" +
                "</html>";

    }
}
