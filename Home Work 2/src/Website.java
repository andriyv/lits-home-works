import java.util.*;
import java.util.stream.Collectors;


public class Website {
    public static void main(String[] args) {
        HomePage homePage = new HomePage("/homepage", "This is Homepage's content");
        LoginPage loginPage = new LoginPage("/login", "Login's Page content");
        ForumPage forumPage1 = new ForumPage("/forum-1", "This is Content of Forum page 1");
        ForumPage forumPage2 = new ForumPage("/forum-2", "This is Content of Forum page 2");
        AboutPage aboutPage1 = new AboutPage("/about-1", "This is Content of About page 1");
        AboutPage aboutPage2 = new AboutPage("/about-1", "This is Content of About page 2");

        List<Page> pages = new ArrayList<>();
        pages.add(homePage);
        pages.add(loginPage);
        pages.add(forumPage1);
        pages.add(forumPage2);
        pages.add(aboutPage1);
        pages.add(aboutPage2);

        System.out.println(pages);

        List<Page> onlyChrome = pages.stream()
                .filter(page -> page instanceof ForumPage).collect(Collectors.toList());
        System.out.println("This is pages which opens only in Chrome: " + onlyChrome);

        List<Page> onlyFirefox = pages.stream()
                .filter(page -> page instanceof AboutPage).collect(Collectors.toList());
        System.out.println("This is pages which opens only in Firefox:  " + onlyFirefox);

        pages.stream().forEach(page -> System.out.println(page.htmlBuilder() + "\n"));


        List<String> Titles = new ArrayList<>();
        pages.stream().forEach(page -> Titles.add(page.getTitle()));
        List<String> distinctTitles = Titles.stream().distinct().collect(Collectors.toList());
        System.out.println(distinctTitles);

        HashMap<String, ArrayList<Page>> mapPages = new HashMap<>();
        for (Page page : pages) {
            if (!mapPages.containsKey(page.getTitle())) {
                mapPages.put(page.getTitle(), new ArrayList<>());
            }
            mapPages.get(page.getTitle()).add(page);
        }
        System.out.println(mapPages);
        for (Map.Entry<String, ArrayList<Page>> entry : mapPages.entrySet()) {
            for(Page page: entry.getValue()){
                System.out.println(page.getHtmlContent());
            }
        }
    }
}



