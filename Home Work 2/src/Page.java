public abstract class Page {
    private String title;
    private String url;
    private String htmlContent;

    public Page(String title, String url, String htmlContent) {
        this.title = title;
        this.url = url;
        this.htmlContent = htmlContent;
    }

    public String getTitle() {
        return title;
    }


    public String getUrl() {
        return url;
    }



    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public void load(){
        System.out.println("Page "  +  getTitle() + " is Loading...");
        System.out.println(htmlBuilder());
    }

    public void refresh(){
        System.out.println("Page " + title + " is refreshing");
        load();
    }

    public abstract String htmlBuilder();
}
