public class AboutPage extends Page implements IFirefox {
    public AboutPage(String url, String htmlContent) {
        super("About Page", url, htmlContent);
    }

    @Override
    public String getHtmlFirefox() {
        return "Firefox";
    }

    @Override
    public String htmlBuilder() {
        return getHtmlFirefox() + "\n" +
                "mysite.com" +  getUrl() +"\n"  +
                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title> " + getTitle() + "</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p> " + getHtmlContent() + " </p>\n" +
                "</body>\n" +
                "</html>";
    }
}
