public class HomePage extends Page {
    public HomePage(String url, String htmlContent) {
        super("Home Page", url, htmlContent);
    }

    @Override
    public String htmlBuilder() {

        return  "mysite.com" +  getUrl() +"\n"  +
                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title> " + getTitle() + "</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p> " + getHtmlContent() + " </p>\n" +
                "</body>\n" +
                "</html>";
    }
}
