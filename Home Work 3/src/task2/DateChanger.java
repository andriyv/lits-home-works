package task2;

import java.time.LocalDate;

public class DateChanger {
    public static void main(String[] args){

    }

    private static LocalDate changeDate(LocalDate currentDate, long years, long months, long days){
        return currentDate.plusYears(years).plusMonths(months).plusDays(days);
    }
}