package task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomStringsCollection {

    public static void main(String[] args) {


    }

    private static List<String> generateRandomStringsCollection(int stringsQuantity) {
        List<String> strings = new ArrayList();
        StringBuilder randomString = new StringBuilder();
        Random rnd = new Random();
        String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for (int i = 0; i < stringsQuantity; i++) {
            int stringLength = rnd.nextInt((15 - 6) + 1) + 6;

            for (int j = 0; j < stringLength; j++) {
                int characterPosition = rnd.nextInt(characters.length());
                randomString = randomString.append(characters.charAt(characterPosition));
            }

            strings.add(randomString.toString());
            randomString.delete(0, randomString.length());
        }

        return strings;
    }
}


