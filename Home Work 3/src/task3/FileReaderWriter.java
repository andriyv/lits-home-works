package task3;


import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class FileReaderWriter {
    public static void main(String[] args) {
    }

    private static void readToMapWriteToFile(String readFilePath, String writeFilePath) {
        HashMap<Integer, String> mapToWrite = readFileToMap(readFilePath);
        writeMapToFile(mapToWrite, writeFilePath);
    }

    private static HashMap<Integer, String> readFileToMap(String filePath) {
        HashMap<Integer, String> stringsFromFile = new HashMap<>();
        int key = 1;
        String line = null;
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(filePath));
            while ((line = bufferedReader.readLine()) != null) {
                stringsFromFile.put(key, line);
                key++;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            filePath + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + filePath + "'");
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
                System.out.println("Problem occured. Can't close reader : " + ex.getMessage());
            }
        }
        return stringsFromFile;
    }

    private static void writeMapToFile(HashMap<Integer, String> mapToWrite, String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                System.out.println("Can not create file " + filePath);
            }
        }
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(filePath));

            for (Map.Entry<Integer, String> entry : mapToWrite.entrySet()) {
                if (entry.getKey() > 0 && ((entry.getKey() & (entry.getKey() - 1)) == 0)) {
                    bw.write(entry.getValue());
                    bw.newLine();
                }
            }
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + filePath + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException ex) {
                System.out.println("Problem occured. Can't close reader : " + ex.getMessage());
            }
        }
    }
}
