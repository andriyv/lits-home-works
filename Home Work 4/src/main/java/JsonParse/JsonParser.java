package JsonParse;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;



public class JsonParser {
    public static void main(String[] args) {

        ObjectMapper mapper = new ObjectMapper();
            Persons persons = null;
//JSON from file to Object
        try {
            persons = mapper.readValue(new File("src/main/java/JsonParse/persons.json"), Persons.class);
            System.out.println(persons);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
