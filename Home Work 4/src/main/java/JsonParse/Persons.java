package JsonParse;

import java.io.Serializable;
import java.util.List;

public class Persons implements Serializable {
    private String title;
    private List<Person> personsList;

    public Persons() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Person> getPersonsList() {
        return personsList;
    }

    public void setPersonsList(List<Person> personsList) {
        this.personsList = personsList;
    }

    @Override
    public String toString() {
        return "Persons { \n" +
                "           title='" + title + "' , \n" +
                "           personList=" + personsList +
                "  }";
    }
}
