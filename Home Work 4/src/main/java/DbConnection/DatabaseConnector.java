package DbConnection;

import java.sql.*;

public class DatabaseConnector {
    // database URL

    static final String DB_URL = "jdbc:mysql://localhost:3306/lits_automation?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "umkaumka";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {

            //STEP 1: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 2: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM users WHERE role = 'user'";
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 3: Extract data from result set
            while (rs.next()) {
                //Retrieve by column name
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String email = rs.getString("email");
                String role = rs.getString("role");

                //Display values
                System.out.print("ID: " + id);
                System.out.print(", Username: " + username);
                System.out.print(", Email: " + email);
                System.out.println(", Role: " + role);
            }
            //STEP 4: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");

    }
}
