package framework.gmail.search;

import framework.gmail.dataprovider.StaticProvider;
import framework.gmail.testcase.TestBaseGmail;
import framework.pages.gmail.GmailSearchResultsPage;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class GmailSearchTestSuite extends TestBaseGmail {
    private GmailSearchResultsPage gmailSearchResultsPage;
    @Test(priority = 1, dataProvider = "gmailtest", dataProviderClass = StaticProvider.class)
    public void notExistingEmailSearchTest(String username, String password, String emailTheme){
                gmailSearchResultsPage = gmailHomePage
                .openSignInPage()
                .openSignInPasswordPage(username)
                .loginToGmail(password)
                .searchEmails(emailTheme);

        Assert.assertTrue(gmailSearchResultsPage.isSearchResultEmpty());
    }

    @Parameters({"email-theme"})
    @Test(priority = 2)
    public void existingEmailSearch(String emailTheme){
        gmailSearchResultsPage.searchEmail(emailTheme);
        Assert.assertTrue(!gmailSearchResultsPage.isSearchResultEmpty());
    }
}
