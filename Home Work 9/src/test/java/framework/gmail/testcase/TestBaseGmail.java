package framework.gmail.testcase;

import framework.pages.LitsPageFactory;
import framework.pages.gmail.GmailHomePage;
import framework.utility.LogFactory;
import framework.utility.PropertyLoader;
import framework.webdriver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;


public class TestBaseGmail {
    private static final Logger LOG = LogFactory.getLogger(TestBaseGmail.class);

    protected WebDriver webDriver;

    protected GmailHomePage gmailHomePage;

    @BeforeClass
    public void setup() {
        String browserName = System.getProperty("browser");
        webDriver = WebDriverFactory.getInstance(browserName);

        LOG.info("Navigating to test url");
        webDriver.get(PropertyLoader.loadProperty("testsite7.url"));

        gmailHomePage = LitsPageFactory.initElements(webDriver, GmailHomePage.class);
    }

    @AfterClass
    public void tearDown() {
        if (webDriver != null) {
            LOG.info("Killing web driver");
            WebDriverFactory.killDriverInstance();
        }
    }
}
