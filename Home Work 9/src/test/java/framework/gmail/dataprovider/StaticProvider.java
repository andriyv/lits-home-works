package framework.gmail.dataprovider;

import org.testng.annotations.DataProvider;

public class StaticProvider {
    @DataProvider(name = "gmailtest")
    public static Object[][] createGmailData() {
        return new Object[][]{
                {"testiquelits", "testique123", "abracadabra"}
        };

    }

}
