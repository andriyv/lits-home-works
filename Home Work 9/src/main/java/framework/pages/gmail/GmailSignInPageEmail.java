package framework.pages.gmail;

import com.google.common.base.Function;
import framework.pages.LitsPageFactory;
import framework.pages.Page;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GmailSignInPageEmail extends Page {
    @FindBy(how = How.ID,using = "identifierId")
    private WebElement emailInputField;

    @FindBy(how = How.ID, using = "identifierNext")
    private WebElement emailNextButton;

    public GmailSignInPageEmail(WebDriver webDriver) {
        super(webDriver);
    }

    @Step("open sing in password page")
    public GmailSignInPagePassword openSignInPasswordPage(String email){
        emailInputField.sendKeys(email);
        emailNextButton.click();

        return LitsPageFactory.initElements(webDriver, GmailSignInPagePassword.class);
    }

    @Override
    public Function<WebDriver, ?> isPageLoaded() {
        return ExpectedConditions.visibilityOf(emailInputField);
    }
}
