package framework.pages.gmail;

import com.google.common.base.Function;
import framework.pages.LitsPageFactory;
import framework.pages.Page;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GmailInboxPage extends Page {

    @FindBy(how = How.ID,using = "gbqfq")
    private WebElement searchInputField;

    @FindBy(how = How.ID, using = "gbqfb")
    private WebElement searchButton;

    public GmailInboxPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Step("Searching an email")
    public GmailSearchResultsPage searchEmails(String emailTheme){
        searchInputField.clear();
        searchInputField.sendKeys(emailTheme);
        searchButton.click();

        return LitsPageFactory.initElements(webDriver, GmailSearchResultsPage.class);
    }

    @Override
    public Function<WebDriver, ?> isPageLoaded() {
        return ExpectedConditions.visibilityOf(searchInputField);
    }
}
