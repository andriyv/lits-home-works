package framework.pages.gmail;

import com.google.common.base.Function;
import framework.pages.LitsPageFactory;
import framework.pages.Page;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GmailHomePage extends Page {

    @FindBy(how = How.XPATH, using = "//*[contains(@class, 'gmail-nav__nav-link__sign-in')]")
    private WebElement signInButton;

    public GmailHomePage(WebDriver webDriver) {
        super(webDriver);
    }

    @Step("Open Sign in Page Email")
    public GmailSignInPageEmail openSignInPage(){
        signInButton.click();
        return LitsPageFactory.initElements(webDriver, GmailSignInPageEmail.class);
    }

    @Override
    public Function<WebDriver, ?> isPageLoaded() {
        return ExpectedConditions.visibilityOf(signInButton);
    }
}
