package framework.pages.gmail;

import com.google.common.base.Function;
import framework.pages.Page;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class GmailSearchResultsPage extends Page {
    @FindBy(how = How.ID,using = "gbqfq")
    private WebElement searchInputField;

    @FindBy(how = How.ID, using = "gbqfb")
    private WebElement searchButton;

    @FindBy(how = How.CSS, using = "div[role = 'main'] *.bog")
    private List<WebElement> gmailSearchResults;

    public GmailSearchResultsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Step("Check search results")
    public boolean isSearchResultEmpty(){
        return gmailSearchResults.isEmpty();
    }

    @Step("Searching an email")
    public GmailSearchResultsPage searchEmail(String emailTheme){
        searchInputField.clear();
        searchInputField.sendKeys(emailTheme);
        searchButton.click();
        return this;
    }


    @Override
    public Function<WebDriver, ?> isPageLoaded() {
        return ExpectedConditions.urlContains("search");
    }
}
