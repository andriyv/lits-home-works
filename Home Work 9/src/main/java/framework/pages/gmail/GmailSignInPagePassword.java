package framework.pages.gmail;

import com.google.common.base.Function;
import framework.pages.LitsPageFactory;
import framework.pages.Page;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GmailSignInPagePassword extends Page {
    @FindBy(how = How.XPATH, using = "//*[contains(@type, 'password')]")
    private WebElement passwordInputField;

    @FindBy(how = How.ID, using = "passwordNext")
    private WebElement passwordNextButton;


    public GmailSignInPagePassword(WebDriver webDriver) {
        super(webDriver);
    }

    @Step("Log in to gmail and open inbox page")
    public GmailInboxPage loginToGmail(String password){
        passwordInputField.click();
        passwordInputField.sendKeys(password);
        passwordNextButton.click();

        return LitsPageFactory.initElements(webDriver,GmailInboxPage.class);
    }

    @Override
    public Function<WebDriver, ?> isPageLoaded() {
        return ExpectedConditions.visibilityOf(passwordInputField);
    }
}
