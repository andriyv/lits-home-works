package task2;

import java.util.*;
import java.util.stream.Collectors;

public class ReversedAndUnique {
	public static void main(String[] args) {
		int[] myArr = {5,2,2,3,7,22,43,55,66,84,84};
		int[] result = reverseAndUnique(myArr);
		System.out.println(Arrays.toString(result));
	}
	
	public static int[] reverseAndUnique(int[] arr) {
		//Reverse Array and print it
		int temp;
		for(int i = 0; i < arr.length/2; i++) {
			temp = arr[arr.length - i - 1];
			arr[arr.length - 1 - i] = arr[i];
			arr[i] = temp;
		}
		System.out.println("Reversed Array: ");
		System.out.println(Arrays.toString(arr) + "\n");
		
		//transform Array to List 
		List<Integer> listFromArray = new ArrayList<>();
		for(int el: arr) {
			listFromArray.add(el);
		}
		//Delete duplicates
		listFromArray = listFromArray.stream().distinct().collect(Collectors.toList());
		
		//List to Array with type int[];
		 arr = new int[listFromArray.size()];
		for(int i = 0; i < arr.length; i++) {
			arr[i] = listFromArray.get(i);
		}
		return arr;
	}
}
