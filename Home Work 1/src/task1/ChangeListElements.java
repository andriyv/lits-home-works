package task1;

import java.util.*;

public class ChangeListElements {
	public static void main(String[] args) {

	}

	public static void changeListElements(List<String> list, String pattern, String changer) {
		int listHalfSize = (int) (list.size() / 2d);
		for (int i = 0; i < listHalfSize; i++) {
			if (list.get(i).equals(pattern)) {
				list.set(i, changer);
			}
		}
		System.out.println(list);
	}
}
