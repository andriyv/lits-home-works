package task3;

import java.util.*;

public class LastMinElementToAverage {
	public static void main(String[] args) {
	}
	
	public static int[] lastMinToAverage(int[] arr) {
		int minValue = Arrays.stream(arr).min().getAsInt();
		int average = (int) Arrays.stream(arr).average().getAsDouble();
		int lastIndex = 0;
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == minValue) {
				lastIndex  = i;
			}
		}
		arr[lastIndex] = average;
		return arr;
	}
}
